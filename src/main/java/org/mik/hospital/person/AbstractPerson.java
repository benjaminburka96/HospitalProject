/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Benji
 *	abstract class AbstractPerson
 */
public abstract class AbstractPerson implements Person {
	public static List<Person> persons = new ArrayList<>();
	Adress adress;

	LocalDate birtDate;

	String firstName;

	String lastName;

	BloodType bloodType;

	/**
	 * 
	 * @param adress
	 *
	 * @param birtDate
	 *
	 * @param firstName
	 *
	 * @param lastName
	 *
	 * @param bloodType
	 * Hozzáadja a persons listához amit létrehoz
	 */

	public AbstractPerson(Adress adress, LocalDate birtDate, String firstName, String lastName, BloodType bloodType) {
		super();
		this.adress = adress;
		this.birtDate = birtDate;
		this.firstName = firstName;
		this.lastName = lastName;
		this.bloodType = bloodType;
		AbstractPerson.persons.add(this);
	}
	/**
	 * Constructor with ResultSet
	 * @param rs
	 * @param adress
	 * @throws Exception
	 */
	public AbstractPerson(ResultSet rs, Adress adress) throws Exception {
		super();
		this.birtDate = rs.getDate(Person.COL_BIRTH_DATE).toLocalDate();
		this.adress = adress;
		this.bloodType = BloodType.fromString(rs.getString(Person.COL_BLOODTYPE));
		this.firstName = rs.getString(Person.COL_FIRST_NAME);
		this.lastName = rs.getString(Person.COL_LAST_NAME);
		AbstractPerson.persons.add(this);
	}

	/**
	 * BloodType típusban visszaadja a vértípust
	 * @return the bloodType[
	 */
	public BloodType getBloodType() {
		return this.bloodType;
	}

	/**
	 * @param bloodType
	 *            the bloodType to set
	 */
	public void setBloodType(BloodType bloodType) {
		this.bloodType = bloodType;
	}

	/**
	 * /**
	 *
	 * @return the adress
	 */
	@Override
	public Adress getAdress() {
		return this.adress;
	}

	/**
	 * @param adress
	 *            the adress to set
	 */
	public void setAdress(Adress adress) {
		this.adress = adress;
	}

	/**
	 * @return the birtDate
	 */
	@Override
	public LocalDate getBirtDate() {
		return this.birtDate;
	}

	/**
	 * @param birtDate
	 *            the birtDate to set
	 */
	public void setBirtDate(LocalDate birtDate) {
		this.birtDate = birtDate;
	}

	/**
	 * @return the firstName
	 */
	@Override
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	@Override
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * A First Name-ből és a Last Name-ből létrehozza a teljes nevet.
	 */
	@Override
	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}
	/**
	 * isDoctor függvénynek false értéket ad
	 */
	@Override
	public boolean isDoctor() {
		return false;
	}
	/**
	 * isStaff függvénynek false értéket ad
	 */
	@Override
	public boolean isStaff() {
		return false;
	}
	/**
	 * isNurse függvénynek false értéket ad
	 */
	@Override
	public boolean isNurse() {
		return false;
	}
	/**
	 * isReceptionist függvénynek false értéket ad
	 */
	@Override
	public boolean isReceptionist() {
		return false;
	}
	/**
	 * isPatient függvénynek false értéket ad
	 */
	@Override
	public boolean isPatient() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.adress == null) ? 0 : this.adress.hashCode());
		result = prime * result + ((this.birtDate == null) ? 0 : this.birtDate.hashCode());
		result = prime * result + ((this.bloodType == null) ? 0 : this.bloodType.hashCode());
		result = prime * result + ((this.firstName == null) ? 0 : this.firstName.hashCode());
		result = prime * result + ((this.lastName == null) ? 0 : this.lastName.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		AbstractPerson other = (AbstractPerson) obj;
		if (this.adress == null) {
			if (other.adress != null) {
				return false;
			}
		} else if (!this.adress.equals(other.adress)) {
			return false;
		}
		if (this.birtDate == null) {
			if (other.birtDate != null) {
				return false;
			}
		} else if (!this.birtDate.equals(other.birtDate)) {
			return false;
		}
		if (this.bloodType != other.bloodType) {
			return false;
		}
		if (this.firstName == null) {
			if (other.firstName != null) {
				return false;
			}
		} else if (!this.firstName.equals(other.firstName)) {
			return false;
		}
		if (this.lastName == null) {
			if (other.lastName != null) {
				return false;
			}
		} else if (!this.lastName.equals(other.lastName)) {
			return false;
		}
		return true;
	}


	@Override
	public String toString() {
		return "AbstractPerson adress=" + this.adress + ", birtDate=" + this.birtDate + ", firstName=" + this.firstName
				+ ", lastName=" + this.lastName;
	}

}
