/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person;

/**
 * @author Benji
 *	Gender enum
 */
public enum Gender {
	
	MALE,FEMALE

}
