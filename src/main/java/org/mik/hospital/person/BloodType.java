/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person;

/**
 * BloodType enum
 *
 * @author Burka Benjamin
 */
public enum BloodType {
	A(1, 1), B(1, 1), AB(1, 1), O(1, 1);

	private int positive;
	private int negative;
	private final static String plus = "+";
	private final static String minus = "-";
	private String name;

	BloodType(int positive, int negative) {
		this.positive = positive;
		this.negative = negative;
		this.name = this.name();
	}

	public BloodType positive() {
		if (this.positive == 1) {
			this.negative = 0;
			this.name = this.name() + BloodType.plus;
			return this;
		} else {
			return null;
		}
	}

	public BloodType negative() {
		if (this.negative == 1) {
			this.positive = 0;
			this.name = this.name() + this.minus;
			return this;
		} else {
			return null;
		}
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Levágja a +-t / --t majd megadja a BloodType-t
	 */
	public static BloodType fromString(String val) throws Exception {
		switch (val.substring(val.length() - 1)) {
		case plus:
			return fromRestrictedBloodType(val.substring(0, val.length() - 1)).positive();
		case minus:
			return fromRestrictedBloodType(val.substring(0, val.length() - 1)).negative();
		default:
			throw new Exception();
		}
	}

	/**
	 * Visszaadja a vércsoportot
	 *
	 * @param val
	 * @return
	 */
	public static BloodType fromRestrictedBloodType(String val) {
		switch (val) {
		case "A":
			return BloodType.A;
		case "B":
			return BloodType.B;
		case "AB":
			return BloodType.AB;
		default:
			return BloodType.O;
		}
	}
}