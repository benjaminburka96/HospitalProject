/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person.staff;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.person.AbstractPerson;
import org.mik.hospital.person.Adress;
import org.mik.hospital.person.BloodType;

/**
 * @author Benji
 *
 */
public abstract class AbstractStaff extends AbstractPerson implements Staff {

	private Integer salary;
	public static List<Staff>staff = new ArrayList<>();

	/**
	 * Extended constructor 
	 * @param adress
	 * @param birtDate
	 * @param firstName
	 * @param lastName
	 * @param bloodType
	 * @param salary
	 */
	public AbstractStaff(Adress adress, LocalDate birtDate, String firstName, String lastName, BloodType bloodType,
			Integer salary) {
		super(adress, birtDate, firstName, lastName, bloodType);
		this.salary = salary;
		staff.add(this);
	}

	
	/**
	 * ResultSet constructor
	 * @param rs
	 * @param adress
	 * @throws Exception
	 */
	public AbstractStaff(ResultSet rs, Adress adress) throws Exception {
		super(rs, adress);
		this.salary=Integer.valueOf(rs.getInt(Staff.COL_SALARY));
		staff.add(this);
	}



	/**
	 * Integer típusban vissza adja a fizetés mennyiségét
	 * @return the salary
	 */
	public Integer getSalary() {
		return this.salary;
	}

	/**
	 * @param salary
	 *            the salary to set
	 */
	public void setSalary(Integer salary) {
		this.salary = salary;
	}
	/**
	 * isStaff metódust felülírja , az alap false értéket true-ra állítja
	 */
	@Override
	public boolean isStaff() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.salary == null) ? 0 : this.salary.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		AbstractStaff other = (AbstractStaff) obj;
		if (this.salary == null) {
			if (other.salary != null) {
				return false;
			}
		} else if (!this.salary.equals(other.salary)) {
			return false;
		}
		return true;
	}

}
