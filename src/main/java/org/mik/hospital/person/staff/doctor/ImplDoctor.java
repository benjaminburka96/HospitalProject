/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person.staff.doctor;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.person.Adress;
import org.mik.hospital.person.BloodType;
import org.mik.hospital.person.staff.AbstractStaff;

/**
 * @author Benji
 * 
 */
public final class ImplDoctor extends AbstractStaff implements Doctor {

	private Specialization specialization;
	public static List<Doctor> doctors = new ArrayList<>();

	/**
	 * @param adress
	 * @param birtDate
	 * @param firstName
	 * @param lastName
	 * @param bloodType
	 * @param salary
	 * @param specialization
	 *            extended constructor
	 */
	public ImplDoctor(Adress adress, LocalDate birtDate, String firstName, String lastName, BloodType bloodType,
			Integer salary, Specialization specialization) {
		super(adress, birtDate, firstName, lastName, bloodType, salary);
		this.specialization = specialization;
		doctors.add(this);
	}

	/**
	 * Constructor with ResultSet
	 * 
	 * @param rs
	 * @param adress
	 * @throws Exception
	 */
	public ImplDoctor(ResultSet rs, Adress adress) throws Exception {
		super(rs, adress);
		this.specialization = Specialization.fromInt(rs.getInt(Doctor.COL_SPECIALIZATION));
		doctors.add(this);
	}

	/**
	 * Visszaadja a Specialization-t Specialization típusban
	 * 
	 * @return the specialization
	 */
	@Override
	public Specialization getSpecialization() {
		return this.specialization;
	}

	/**
	 * @param specialization
	 *            the specialization to set
	 */
	public void setSpecialization(Specialization specialization) {
		this.specialization = specialization;
	}

	/**
	 * felülírja az isDoctor() függvényt , az alap false értéket, true-ra állítja
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see org.mik.hospital.person.AbstractPerson#isDoctor()
	 */
	@Override
	public boolean isDoctor() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.specialization == null) ? 0 : this.specialization.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		ImplDoctor other = (ImplDoctor) obj;
		if (this.specialization != other.specialization) {
			return false;
		}
		return true;
	}

}
