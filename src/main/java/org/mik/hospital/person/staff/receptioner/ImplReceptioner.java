/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.person.staff.receptioner;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.person.Adress;
import org.mik.hospital.person.BloodType;
import org.mik.hospital.person.staff.AbstractStaff;

/**
 * @author Benji final class Receptioner
 */
public final class ImplReceptioner extends AbstractStaff implements Receptioner {

	private Integer phoneNumber;
	public static List<Receptioner> receptioners = new ArrayList<>();

	/**
	 * extended constructor
	 * 
	 * @param adress
	 * @param birtDate
	 * @param firstName
	 * @param lastName
	 * @param bloodType
	 * @param salary
	 * @param phoneNumber
	 */
	public ImplReceptioner(Adress adress, LocalDate birtDate, String firstName, String lastName, BloodType bloodType,
			Integer salary, Integer phoneNumber) {
		super(adress, birtDate, firstName, lastName, bloodType, salary);
		this.phoneNumber = phoneNumber;
		receptioners.add(this);
	}

	/**
	 * Constructor with ResultSet
	 * 
	 * @param rs
	 * @param adress
	 * @throws Exception
	 */
	public ImplReceptioner(ResultSet rs, Adress adress) throws Exception {
		super(rs, adress);
		this.phoneNumber = Integer.valueOf(rs.getInt(Receptioner.COL_PHONE_NUMBER));
		receptioners.add(this);
	}

	/**
	 * Integer típúsban visszaadja a telefonszámot
	 * 
	 * @return the phoneNumber
	 */
	@Override
	public Integer getPhoneNumber() {
		return this.phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * felülírja az isReceptionist() függvényt , az alap false értéket , true-ra
	 * állítja
	 */
	@Override
	public boolean isReceptionist() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.phoneNumber == null) ? 0 : this.phoneNumber.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		ImplReceptioner other = (ImplReceptioner) obj;
		if (this.phoneNumber == null) {
			if (other.phoneNumber != null) {
				return false;
			}
		} else if (!this.phoneNumber.equals(other.phoneNumber)) {
			return false;
		}
		return true;
	}

}
