/**
 *Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.treatment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * abstract class for treatments
 *
 * @author Burka Benjamin
 *
 */
public abstract class AbstractTreatment implements Treatment {
	public static List<Treatment> allTreatments = new ArrayList<>();
	private String name;

	/**
	 * constructor for treatments
	 *
	 * @param name
	 */
	public AbstractTreatment(String name) {
		super();
		this.name = name;
		allTreatments.add(this);
	}

	/**
	 * resultsetes constructor
	 *
	 * @param rs
	 * @throws SQLException
	 */
	public AbstractTreatment(ResultSet rs) throws SQLException {
		super();
		this.name = rs.getString(Treatment.COL_NAME);
		allTreatments.add(this);
	}

	/**
	 * gets the name
	 *
	 * @return name
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * sets the name
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * alapértéket ad az isSurgeonnak
	 */
	@Override
	public boolean isSurgeon() {
		return false;
	}

	/**
	 * alapértéket ad az isMedicine()-nek
	 */
	@Override
	public boolean isMedicine() {
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		AbstractTreatment other = (AbstractTreatment) obj;
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		return true;
	}

}
