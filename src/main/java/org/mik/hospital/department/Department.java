/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.department;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.person.patient.Patient;
import org.mik.hospital.person.staff.Staff;
import org.mik.hospital.treatment.Treatment;

/**
 * @author Burka Benjamin Department class
 */
public class Department {

	public final static String TABLE_NAME = "department";
	public final static String COL_ID = "id";
	public final static String COL_NAME = "name";

	private String name;
	private List<Staff> staff;
	private List<Patient> patients;
	private List<Treatment> treatments;
	public static List<Department> departments = new ArrayList<>();

	/**
	 * @param name
	 * @param staff
	 * @param patients
	 * @param treatments
	 *            hozzáadja a departments listához
	 */
	public Department(String name, List<Staff> staff, List<Patient> patients, List<Treatment> treatments) {
		super();
		this.name = name;
		this.staff = staff;
		this.patients = patients;
		this.treatments = treatments;
		Department.departments.add(this);
	}

	/**
	 * ResultSet constructor
	 *
	 * @param rs
	 * @throws SQLException
	 */
	public Department(ResultSet rs) throws SQLException {
		super();
		this.name = rs.getString(Department.COL_NAME);
		this.staff = new ArrayList<>();
		this.patients = new ArrayList<>();
		this.treatments = new ArrayList<>();
		Department.departments.add(this);
	}

	/**
	 * hozzáadja a kezelést a kezeléslistához
	 *
	 * @param treatment
	 */
	public void addTreatment(Treatment treatment) {
		this.treatments.add(treatment);
	}

	/**
	 * hozzáadja a Pácienst a páciens listához
	 *
	 * @param patient
	 */
	public void addPatient(Patient patient) {
		this.patients.add(patient);
	}

	/**
	 * hozzáadja a Személyzetet a személyzet listához
	 *
	 * @param staff
	 */
	public void addStaff(Staff staff) {
		this.staff.add(staff);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the staff
	 */
	public List<Staff> getStaff() {
		return this.staff;
	}

	/**
	 * @param staff
	 *            the staff to set
	 */
	public void setStaff(List<Staff> staff) {
		this.staff = staff;
	}

	/**
	 * @return the patients
	 */
	public List<Patient> getPatients() {
		return this.patients;
	}

	/**
	 * @param patients
	 *            the patients to set
	 */
	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}

	/**
	 * @return the treatments
	 */
	public List<Treatment> getTreatments() {
		return this.treatments;
	}

	/**
	 * @param treatments
	 *            the treatments to set
	 */
	public void setTreatments(List<Treatment> treatments) {
		this.treatments = treatments;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.patients == null) ? 0 : this.patients.hashCode());
		result = prime * result + ((this.staff == null) ? 0 : this.staff.hashCode());
		result = prime * result + ((this.treatments == null) ? 0 : this.treatments.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Department other = (Department) obj;
		if (this.patients == null) {
			if (other.patients != null) {
				return false;
			}
		} else if (!this.patients.equals(other.patients)) {
			return false;
		}
		if (this.staff == null) {
			if (other.staff != null) {
				return false;
			}
		} else if (!this.staff.equals(other.staff)) {
			return false;
		}
		if (this.treatments == null) {
			if (other.treatments != null) {
				return false;
			}
		} else if (!this.treatments.equals(other.treatments)) {
			return false;
		}
		return true;
	}

}
