/**
 *  Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.disease.diagnosis.virus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.disease.diagnosis.AbstractDiagnosis;
import org.mik.hospital.disease.diagnosis.Seriousness;
import org.mik.hospital.disease.symptoms.Symptoms;

/**
 * final class Virus
 *
 * @author Burka Benjamin
 */
public final class Virus extends AbstractDiagnosis {
	public static List<Virus> virus = new ArrayList<>();

	/**
	 * @param name
	 * @param seriousness
	 * @param symptoms
	 *            hozzáadja a virus listához
	 */
	public Virus(String name, Seriousness seriousness, List<Symptoms> symptoms) {
		super(name, seriousness, symptoms);
		virus.add(this);
	}

	/**
	 * resultSetes constructor
	 *
	 * @param resultSet
	 * @param symptoms
	 * @throws SQLException
	 */
	public Virus(ResultSet resultSet, List<Symptoms> symptoms) throws SQLException {
		super(resultSet, symptoms);
		virus.add(this);
	}

	/**
	 * true-ra állítja
	 */
	@Override
	public boolean isVirus() {
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " vírus vagyok\n";
	}
}
