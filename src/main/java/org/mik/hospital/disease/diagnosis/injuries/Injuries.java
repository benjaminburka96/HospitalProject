/**
 * Burka Benjamin [C1DK6R]
 */
package org.mik.hospital.disease.diagnosis.injuries;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mik.hospital.disease.diagnosis.AbstractDiagnosis;
import org.mik.hospital.disease.diagnosis.Seriousness;
import org.mik.hospital.disease.symptoms.Symptoms;

/**
 * @author Burka Benjamin
 */
public final class Injuries extends AbstractDiagnosis {
	public static List<Injuries> injuries = new ArrayList<>();

	/**
	 * @param name
	 * @param seriousness
	 * @param symptoms
	 *            Listához adja
	 */
	public Injuries(String name, Seriousness seriousness, List<Symptoms> symptoms) {
		super(name, seriousness, symptoms);
		injuries.add(this);
	}

	/**
	 * Resultsetes constructor
	 *
	 * @param rs
	 * @param symptoms
	 * @throws SQLException
	 */
	public Injuries(ResultSet rs, List<Symptoms> symptoms) throws SQLException {
		super(rs, symptoms);
		injuries.add(this);
	}

	/**
	 * felülírja az isInjury metódust
	 */
	@Override
	public boolean isInjury() {
		return true;
	}

	@Override
	public boolean isDiagnosis() {
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " Type: Injury\n";
	}

}
