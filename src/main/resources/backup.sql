-- MySQL dump 10.16  Distrib 10.1.31-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: hospital
-- ------------------------------------------------------
-- Server version	10.1.31-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hospital`
--

/*!40000 DROP DATABASE IF EXISTS `hospital`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hospital` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hospital`;

--
-- Table structure for table `adress`
--

DROP TABLE IF EXISTS `adress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  `state` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  `city` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  `postalcode` int(11) NOT NULL,
  `street` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf32 COLLATE=utf32_hungarian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adress`
--

LOCK TABLES `adress` WRITE;
/*!40000 ALTER TABLE `adress` DISABLE KEYS */;
INSERT INTO `adress` VALUES (2,'United States of America','Oklahoma','Stillwater',12323,'Big'),(3,'Hungary','Baranya','Pécs',7600,'Boszorkány'),(4,'United States of America','Texas','Texas',12212,'Great'),(5,'United States of America','Florida','Florida',232,'65'),(6,'United States of America','Alaszka','Ferot',234,'Larger');
/*!40000 ALTER TABLE `adress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf32 COLLATE=utf32_hungarian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'radiológia'),(2,'Accident and emergency'),(3,'Cardiology'),(4,'General surgery\r\n'),(5,'Microbiology\r\n');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diagnosis`
--

DROP TABLE IF EXISTS `diagnosis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diagnosis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  `seriousness` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  `treatment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_key_treatment_id` (`treatment_id`),
  CONSTRAINT `fk_key_treatment_id` FOREIGN KEY (`treatment_id`) REFERENCES `treatment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf32 COLLATE=utf32_hungarian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnosis`
--

LOCK TABLES `diagnosis` WRITE;
/*!40000 ALTER TABLE `diagnosis` DISABLE KEYS */;
INSERT INTO `diagnosis` VALUES (3,'Rák',1,'Virus',1),(4,'Broken arm',0,'Injury',4),(5,'Tuberculosis',2,'Medicine',2),(6,'Rabies',1,'Virus',3),(7,'Cancer',2,'Virus',3),(8,'Pneumonia',2,'Bacteria',5),(9,'Cancerous tumor in the bone or muscle of the limb\r\n',2,'Virus',7),(10,'Hearth attack',2,'Injury',8);
/*!40000 ALTER TABLE `diagnosis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diagnosis_symptoms`
--

DROP TABLE IF EXISTS `diagnosis_symptoms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diagnosis_symptoms` (
  `diagnosis_id` int(11) NOT NULL,
  `symptoms_id` int(11) NOT NULL,
  KEY `fk_diag` (`diagnosis_id`),
  KEY `fk_symp` (`symptoms_id`),
  CONSTRAINT `fk_diag` FOREIGN KEY (`diagnosis_id`) REFERENCES `diagnosis` (`id`),
  CONSTRAINT `fk_symp` FOREIGN KEY (`symptoms_id`) REFERENCES `symptoms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_hungarian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnosis_symptoms`
--

LOCK TABLES `diagnosis_symptoms` WRITE;
/*!40000 ALTER TABLE `diagnosis_symptoms` DISABLE KEYS */;
INSERT INTO `diagnosis_symptoms` VALUES (4,4),(4,12),(4,13),(4,15),(5,3),(5,6),(5,7),(6,3),(6,7),(6,14),(7,4),(7,6),(7,8),(7,10),(7,13),(8,7),(8,14),(9,13),(9,14),(10,14);
/*!40000 ALTER TABLE `diagnosis_symptoms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  `bloodtype` varchar(3) COLLATE utf32_hungarian_ci NOT NULL,
  `birth_date` date NOT NULL,
  `adress_id` int(11) NOT NULL,
  `salary` int(11) DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `number_of_patient` int(11) DEFAULT NULL,
  `specialization` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `diagnosis_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_depid` (`department_id`),
  KEY `fk_adress` (`adress_id`),
  KEY `fk_diagnosis` (`diagnosis_id`),
  CONSTRAINT `fk_adress` FOREIGN KEY (`adress_id`) REFERENCES `adress` (`id`),
  CONSTRAINT `fk_depid` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `fk_diagnosis` FOREIGN KEY (`diagnosis_id`) REFERENCES `diagnosis` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf32 COLLATE=utf32_hungarian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (15,'Matthew','Anderson','A-','1997-01-12',4,NULL,NULL,NULL,NULL,NULL,NULL),(16,'Zoar','Taker','AB+','1944-10-21',3,30000,421321323,NULL,NULL,3,NULL),(17,'Daiys','Heath','0+','1995-03-30',6,20000,NULL,4,NULL,2,NULL),(18,'Juliett','Baker','B-','1992-02-12',2,32000,NULL,NULL,2,4,NULL),(19,'Kristin','Folks','A-','1999-04-12',6,NULL,NULL,NULL,NULL,NULL,NULL),(20,'Cloe','Uthay','AB+','2001-11-10',2,32000,NULL,0,NULL,3,NULL),(21,'Ellen','Jordan','A-','1976-03-23',3,25000,NULL,NULL,1,5,NULL),(22,'Bison','Juliett','AB+','1999-01-12',5,31000,NULL,2,NULL,3,NULL);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `symptoms`
--

DROP TABLE IF EXISTS `symptoms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `symptoms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf32 COLLATE=utf32_hungarian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `symptoms`
--

LOCK TABLES `symptoms` WRITE;
/*!40000 ALTER TABLE `symptoms` DISABLE KEYS */;
INSERT INTO `symptoms` VALUES (1,'hasfájás'),(2,'fejfájás'),(3,'Fatigue'),(4,'Body aches '),(5,'Cough'),(6,'Sore throat'),(7,'Fever'),(8,'Gastrointestinal problems'),(9,'Nausea'),(10,'Diarrhea'),(11,'Sore Throat'),(12,'Muscle and Joint Pain\r\n'),(13,'Deformity'),(14,'Severe pain'),(15,'Swelling');
/*!40000 ALTER TABLE `symptoms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `treatment`
--

DROP TABLE IF EXISTS `treatment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `treatment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  `price` int(11) NOT NULL,
  `body_part` varchar(255) COLLATE utf32_hungarian_ci NOT NULL,
  `department_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_depid2` (`department_id`),
  CONSTRAINT `fk_depid2` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf32 COLLATE=utf32_hungarian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `treatment`
--

LOCK TABLES `treatment` WRITE;
/*!40000 ALTER TABLE `treatment` DISABLE KEYS */;
INSERT INTO `treatment` VALUES (1,'asdasd',1,'asd',1),(2,'Aminoglycoside antibiotics',300,'',5),(3,'Cephalosporin antibiotics\r\n',200,'',5),(4,'Splinting the broken arm',0,'Arm',2),(5,'Specific antiviral treatments',500,'',5),(6,'Chest Surgeon',0,'Chest',4),(7,'Limb Surgeon',0,'Leg',4),(8,'Improving hearth status',50,'',3);
/*!40000 ALTER TABLE `treatment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-01 12:58:24
